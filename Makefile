CC=g++
PKG_CONFIG=-lsfml-graphics -lsfml-window -lsfml-system
OUTPUTDIR=./output/
INCLUDES=-I./includes/
SOURCES=./src/*.cpp
EXENAME=game

make:
	$(CC) $(SOURCES) $(INCLUDES) $(PKG_CONFIG) $(EXTERNAL) -o $(OUTPUTDIR)$(EXENAME)

run:
	$(OUTPUTDIR)$(EXENAME)
