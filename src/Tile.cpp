#include "Tile.hpp"

Tile::Tile( void ) {
	// Created 
}

bool Tile::init( sf::Texture &tex ) {
	// Grab the reference to the texture it'll use
	if ( &tex == nullptr ) {
		return false;
	} else {
		t_tile = &tex;
	}

	// Setup tile's render rectangle on the texture
    renRect_tile.left =     0;
    renRect_tile.top =      0;
    renRect_tile.width =    16;
    renRect_tile.height =   16;

   	s_tile.setTexture( *t_tile );
	s_tile.setTextureRect( renRect_tile );

	s_tile.setPosition( 0, 128 );
	renRect_tile.top =      128;

    // Setup tile's bounding box to be the size of the texture
    bbRect_tile.left =      renRect_tile.left;
    bbRect_tile.top =       renRect_tile.top;
    bbRect_tile.width =     renRect_tile.width;
    bbRect_tile.height =    renRect_tile.height;

	return true;
}

bool Tile::initWithPosition( sf::Texture &tex, float xPos, float yPos ) {
	// Grab the reference to the texture it'll use
	if ( &tex == nullptr ) {
		return false;
	} else {
		t_tile = &tex;
	}

	// Setup tile's render rectangle on the texture
    renRect_tile.left =     0;
    renRect_tile.top =      0;
    renRect_tile.width =    16;
    renRect_tile.height =   16;

   	s_tile.setTexture( *t_tile );
	s_tile.setTextureRect( renRect_tile );

	s_tile.setPosition( xPos, yPos );
	renRect_tile.top =      yPos;

    // Setup tile's bounding box to be the size of the texture
    bbRect_tile.left =      xPos;
    bbRect_tile.top =       yPos;
    bbRect_tile.width =     renRect_tile.width;
    bbRect_tile.height =    renRect_tile.height;

	return true;
}
sf::Sprite Tile::getSprite( void ) const {
	return s_tile;
}

sf::Rect<float> Tile::getBoundingBox( void ) const {
	return bbRect_tile;
}
