#include "Game.hpp"

Game::Game( void ) {
    //MAX_TILES =     16;
	gravity =       9.81f;
	accel =         10.0f;
    friction =      8.8f;
    maxVel =        3.0f;
    hVel = vVel =   0.0f;
    jumpForce =     -200.0f;
    isGrounded =    false;
	scale =         1;
	width =         400; // Better widescreen support
	height =        240;
	title =         "Project Jump v2";
}

bool Game::init( void ) {
	/**** Window Setup ****/
	// Create game window
	window.create( sf::VideoMode( width * scale, height * scale ), title );
	// Set max framerate and vertical sync on
	window.setFramerateLimit( 60 );
	window.setVerticalSyncEnabled( true );

    /**** Media Loading ****/
	// Attempt to load character texture
	if( !t_characters.loadFromFile( "./Data/player.png" ) ) {
		std::cout << "Could not load character texture" << std::endl;
		return false;
	}

	// Attempt to load tile texture
	if( !t_tiles.loadFromFile( "./Data/tile.png" ) ) {
		std::cout << "Could not load tile texture" << std::endl;
		return false;
	}

    // Attempt to load font
	if( !font.loadFromFile( "./Data/ARCADECLASSIC.TTF" ) ) {
			std::cout << "Could not load font" << std::endl;
			return false;
	}

    /**** Object Initialization ****/
	// Attempt to initialize Player
	if( !player.init( t_characters ) ) {
		std::cout << "Could not create player" << std::endl;
		return false;
	}

	// Attempt to initialize tiles
    for( int t = 0; t < MAX_TILES - 1; t++) {
	    if( !tiles[ t ].initWithPosition( t_tiles, t * 16, 128 ) ) {
		    std::cout << "Could not create tile" << std::endl;
		    return false;
        }
	}

	/**** Text Setup ****/
	// Setup each text's font style
	debug.setFont( font );
	timer.setFont( font );

	// Setup each text's string
	debug.setString( "DEBUG\nSpeed: " + std::to_string( hMove ) + "\nisGrounded: " + std::to_string( isGrounded ) );
	timer.setString( "DELTA TIME: " + std::to_string( clock.getElapsedTime().asSeconds() ) );

	// Setup each text's size
	debug.setCharacterSize( 11 );
	timer.setCharacterSize( 11 );

	// Setup each text's position
	debug.setPosition( 260, 2 );
	timer.setPosition( 260, 34 );

	return true;
}

void Game::run( void )
{
	// Main Game Loop
    while ( window.isOpen() )
    {
    	double delta = clock.restart().asSeconds();

        // Event Handling
        sf::Event event;
        while ( window.pollEvent( event ) ) {
            // If we request the window to close
            if ( event.type == sf::Event::Closed ) {
                window.close();
			}
        }

        // Controls
        control(delta);

        // Update the debug and timer text
        debug.setString( "DEBUG\nSpeed: " + std::to_string( hMove ) + "\nisGrounded: " + std::to_string( isGrounded ));
        timer.setString( "DELTA TIME: " + std::to_string( clock.getElapsedTime().asSeconds() ) );

        // Draw code
        draw();
    } 
}

void Game::draw( void ) {
	// Light Blue clear colors
    window.clear( sf::Color( 85, 145, 255 ) );
    window.draw( debug );
    window.draw( timer );
    window.draw( player.getSprite() );
    // Draw all the tiles
    for(int i = 0; i < MAX_TILES - 1; i++) {
        window.draw( tiles[ i ].getSprite() );
    }

    window.draw( player.getDebugBoundingBox() ); // Needs to be the last thing drawn
    window.display();
}

void Game::control( double delta ) {
    // Gravity always keepin ya down
    vVel += gravity * delta;

    // Left, Right, and when not moving
    if ( sf::Keyboard::isKeyPressed( sf::Keyboard::D ) ) {     
        hVel += accel * delta;
    } else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::A ) ) {   
        hVel -= accel * delta;
    } else {                                                    
        // Constantly decrease our speed when not walking/running
        if( hVel > 0 ) {
            /* Threshold, so it doesn't go back and forth between slightly bigger than 0 and
             * slightly less than 0*/
            if( hVel < 0.1 ) {
                hVel = 0; 
            } else {
                hVel -= friction * delta;
            }
        } else if( hVel < 0 ) {
            if( hVel > -0.1 ) {
                hVel = 0;
            } else {
                hVel += friction * delta;
            }
        }
    }
    // Jump
    if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Space ) && isGrounded ) {
        vVel = jumpForce * delta;
        isGrounded = false;
    }

    // If the player isn't hitting a tile, let gravity do its thing
    // This Rect of for precalculating expected collision
    sf::Rect<float> spaceBefore( player.getBoundingBox().left,
                                 player.getBoundingBox().top,
                                 player.getBoundingBox().width + hVel,
                                 player.getBoundingBox().height + vVel );

    // TODO: This will probably end up being a hefty process as more tiles are added to a level
    // Consider making it more efficient later
    for(int i = 0; i < MAX_TILES - 1; i++) {
        if( spaceBefore.intersects( tiles[ i ].getBoundingBox() ) ) {
            vVel = 0;
            isGrounded = true;
        }
    }

    // Cap the possible speed
    hVel = std::max( std::min( hVel, maxVel ), -maxVel );

    hMove = hVel;
    vMove = vVel;
    // Move the player
    player.move( hMove, vMove );
}
