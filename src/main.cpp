#include "Game.hpp"

int main( void ) {
    Game game;

    if( !game.init() ) {
        return EXIT_FAILURE;
    } else {
        game.run();
    }

    return 0;
}
