#include "Player.hpp"

Player::Player( void ) {
	// Created
}

bool Player::init( sf::Texture &tex ) {
	// Grab the reference to the texture it will use
	if ( &tex == nullptr ) {
		return false;
	} else {
		t_player = &tex;
	}

  // Setup player's Render Rectangle from the texture
  renRect_player.left =     0;
  renRect_player.top =      0;
  renRect_player.width =    16;
  renRect_player.height =   32;

  // Setup the player's sprite
  s_player.setTexture( *t_player );
  s_player.setTextureRect( renRect_player );
  s_player.setPosition( 0, 0 );

  // Setup the player's bounding box
  bb_player.left =      s_player.getPosition().x + 2;
  bb_player.top =       s_player.getPosition().y;
  bb_player.width =     renRect_player.width - 2;
  bb_player.height =    renRect_player.height;

  // Setup the player's debug draw box
  dbg_player.setSize( sf::Vector2f( bb_player.width, bb_player.height ) );
  // Set its actual position based on the player's bounding box
  dbg_player.setPosition( bb_player.left, bb_player.top );
  dbg_player.setFillColor( sf::Color( 0, 0, 0, 0 ) );
  dbg_player.setOutlineColor( sf::Color( 0, 255, 0 ) );
  dbg_player.setOutlineThickness( 1 );

  return true;
}

sf::Sprite Player::getSprite( void ) const {
	return s_player;
}

sf::Rect<float> Player::getBoundingBox(void ) const {
	return bb_player;
}

sf::RectangleShape Player::getDebugBoundingBox( void ) const {
	return dbg_player;
}

void Player::move( float xAmount, float yAmount ) {
	s_player.move( xAmount, yAmount );
	bb_player.left = s_player.getPosition().x + 2;
    bb_player.top = s_player.getPosition().y;
    dbg_player.setPosition( bb_player.left, bb_player.top );
}
