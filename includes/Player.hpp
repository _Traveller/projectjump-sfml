#pragma once
#include <SFML/Graphics.hpp>

class Player {
public:
	Player();
	bool                init( sf::Texture &tex );
	sf::Sprite          getSprite( void ) const;
	sf::Rect<float>     getBoundingBox( void ) const;
	sf::RectangleShape  getDebugBoundingBox( void ) const;
	void                move( float xAmount, float yAmount );

private:
	// Player specific variables
	sf::Texture         *t_player;
	sf::Sprite          s_player;
	sf::Rect<float>     bb_player;
	sf::Rect<int>       renRect_player;
	sf::RectangleShape  dbg_player;

	// TODO: add player specific variables like health and lives

};
