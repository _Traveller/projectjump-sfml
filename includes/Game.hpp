#pragma once

#include <iostream>
#include <SFML/Graphics.hpp>
#include "Player.hpp"
#include "Tile.hpp"

// TODO: Unsafe, but I just want it to fucking compile for now
// Makes a line of tiles across the screen
#define MAX_TILES ( 416 / 16 )

class Game {
public:
	Game( void );
	bool                init();
	void                run();

private:
	// Game Window
	sf::RenderWindow    window;
	sf::Clock           clock;
	sf::Font            font;
	sf::Text            debug;
	sf::Text            timer;

	// Textures
	sf::Texture         t_characters;
	sf::Texture         t_tiles;

	// Player
	Player              player;

	// Tile Data
	Tile                tiles[MAX_TILES];

	// Render and Window variables
	int                 scale;
	int                 width;
	int                 height;
	std::string         title;

	// Gameplay Variables
	float               gravity;
    float               accel;
    float               friction;
	float               hVel, vVel;
    float               jumpForce;
    bool                isGrounded;
    float               maxVel;
	float               hMove, vMove;

	void                draw( void );
	void                control( double delta );

};
