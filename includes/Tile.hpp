#pragma once
#include <SFML/Graphics.hpp>

class Tile {
public:
	Tile( void );
	bool            init( sf::Texture &tex );
    bool            initWithPosition( sf::Texture &text, float xPos, float yPos );
	sf::Sprite      getSprite( void ) const;
	sf::Rect<float> getBoundingBox( void ) const;

private:
	sf::Texture     *t_tile;
	sf::Sprite      s_tile;
	sf::Rect<int>   renRect_tile;
    sf::Rect<float> bbRect_tile;

};
